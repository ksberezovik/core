import React from "react";
import {BrowserRouter as Router, Route, Link, Switch} from "react-router-dom";
import {CategoryList} from "./Component/Category/CategoryList.jsx";
import {RoleCheckbox, SignUp} from "./Component/SignUp/Role.jsx";
import {Category} from "./Component/Category/Category.jsx";
import {Schedule} from "./Component/Schedule/Schedule.jsx";
import {
    Layout, Menu, Breadcrumb, Icon, Button,
} from 'antd';
import "./App.css"
import {LoginForm} from "./Component/SignUp/LoginForm.jsx";
import {HomeUser} from "./Component/User/HomeUser.jsx";
import {TokenStorage} from "./Services/TokenStorage.js";
import {SessionStorage} from "./Services/SessionStorage.js";
import {IndexPage} from "./Component/IndexPage/IndexPage.jsx";
import {DoctorDetail} from "./Component/Doctor/DoctorDetail.jsx";
import {DoctorPanel} from "./Component/Doctor/DoctorPanel.jsx";
import {DoctorList} from "./Component/Doctor/DoctorList.jsx";
import {DoctorItemDetails} from "./Component/Doctor/DoctorItemDetails.jsx";


const {SubMenu} = Menu;
const {
    Header, Content, Footer, Sider,
} = Layout;

export class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: TokenStorage.getToken() !== null,
            user: SessionStorage.getUsername(),
            authToken: TokenStorage.getToken(),
            role: SessionStorage.getRole(),
        };

        this.loginUser = this.loginUser.bind(this);
        this.logoutUser = this.logoutUser.bind(this);
        this.setRole = this.setRole.bind(this);
    }

    loginUser(authToken, username) {
        TokenStorage.setToken(authToken);
        SessionStorage.setUsername(username);

        this.setState({
            isLoggedIn: true,
            user: username,
            authToken,
        })
    }

    setRole(role) {
        this.setState({role});
    }

    logoutUser() {
        SessionStorage.logOut();

        this.setState({
            isLoggedIn: false,
            user: null,
            authToken: null,
            role: null,
        });
    }

    render() {
        const {role} = this.state;

        return <Router>
            <Layout>
                <Header className="header">
                    <div className="logo"/>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        style={{lineHeight: '64px'}}>
                        <Menu.Item key="1"><Link to="/">Главная</Link></Menu.Item>
                        {
                            !this.state.isLoggedIn && (
                                <Menu.Item key="2"><Link to="/login-new">Вход</Link></Menu.Item>
                            )
                        }
                        <Menu.Item key="3"><Link to="/signUp">Регистрация</Link></Menu.Item>
                        {
                            role !== null && SessionStorage.isPatient(role) && (
                                <Menu.Item key="4"><Link to="/home-user">Личный кабинет пациент</Link></Menu.Item>
                            )
                        }
                        {
                            role !== null && SessionStorage.isDoctor(role) && (
                                <Menu.Item key="5"><Link to="/home-doctor">Личный кабинет доктор</Link></Menu.Item>
                            )
                        }
                        {
                            this.state.isLoggedIn && (
                                <Menu.Item key="6" >
                                    <Button
                                        icon={"user"}
                                        onClick={this.logoutUser}
                                        type="danger"
                                        ghost
                                    >
                                        Выйти
                                    </Button>
                                </Menu.Item>
                            )
                        }
                    </Menu>
                </Header>
                <Content style={{padding: '0 50px', minHeight: 650}} >
                    <Layout style={{padding: '24px 0', background: '#fff'}}>
                        <Sider width={200} style={{background: '#fff'}}>
                            <Menu
                                mode="inline"
                                defaultOpenKeys={['sub1']}
                                style={{height: '100%'}}>
                                <Menu.Item key="1"><Link to="/categories">Специализации</Link></Menu.Item>
                                <Menu.Item key="2"><Link to="/doctors">Врачи</Link></Menu.Item>

                            </Menu>
                        </Sider>
                        <Content style={{padding: '0 24px', minHeight: 500}}>
                            <Switch>
                                <Route path="/" exact component={IndexPage}/>
                                <Route path="/categories" component={CategoryList}/>
                                <Route path="/login-new" render={(props) => <LoginForm setRole={this.setRole} loginUser={this.loginUser} {...props}/>}/>
                                <Route path="/category/:id" component={Category}/>
                                <Route path="/signUp" component={RoleCheckbox}/>
                                <Route path="/doctor/:doctorId" component={DoctorDetail}/>
                                <Route path="/schedule/:doctorId" component={Schedule}/>
                                <Route path="/doctors" component={DoctorList}/>
                                <Route path="/home-user" component={HomeUser}/>
                                <Route path="/home-doctor" component={DoctorPanel}/>
                                <Route path="/doctor-item-details/:id" component={DoctorItemDetails}/>
                            </Switch>
                        </Content>
                    </Layout>
                </Content>
                <Footer style={{textAlign: 'center'}}>
                    BSUIR ©2019
                </Footer>
            </Layout>
        </Router>
    }
}
