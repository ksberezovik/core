import React from "react";
import {PatientSignUp} from "./PatientSignUp.jsx";
import {DoctorSignUp} from "./DoctorSignUp.jsx";

export function RenderForm(props) {
    const showPatientForm = props.showPatientForm;
    const showDoctorForm = props.showDoctorForm;

    if (showPatientForm) {
        return <PatientSignUp/>;
    }

    if (showDoctorForm) {
        return <DoctorSignUp/>;
    }
}

export function CategoryOption(props) {
    const categories = props.categories;
    console.log("test");
    console.log(categories);
    let array = [];
    for(let i = 0; i < categories.length; i++) {
        array.push(
            <option key={i} value={categories[i].name}>{categories[i].name}</option>
        );
    }
    return array;
}