import React from "react";
import {CategoryOption} from "./Utils.jsx";
import {TokenStorage} from "../../Services/TokenStorage.js";

export class DoctorSignUp extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            middleName: '',
            secondName: '',
            category: '',
            categories: [],
            login: '',
            password: ''
        };
        this.onFieldChange = this.onFieldChange.bind(this);
        this.handleSignUp = this.handleSignUp.bind(this);
    };

    onFieldChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name] : value
        });
    }

    componentDidMount() {
        fetch("http://localhost:8080/categories/all")
            .then(response => response.json())
            .then(categories => this.setState({categories: categories, category: categories[0].name}))
            .catch(err => this.setState({error: "Server error!"}));
    }

    handleSignUp(e) {
        e.preventDefault();

        fetch('http://localhost:8080/users/doctor/signup', {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                name: {
                    firstName: this.state.firstName,
                    middleName: this.state.middleName,
                    secondName: this.state.secondName
                },
                category: {
                    name: this.state.category
                },
                username: this.state.login,
                password: this.state.password,
            }),
        })
            .then(request => request.text())
            .then(d => console.log(d));
    }

    render() {
        return (
            <div>
                <form>
                    <p><label>Имя:<input type="text" onChange={this.onFieldChange} name="firstName" value={this.state.firstName}/></label></p>
                    <p><label>Отчество:<input type="text" onChange={this.onFieldChange} name="middleName" value={this.state.middleName}/></label></p>
                    <p><label>Фамилия:<input type="text" onChange={this.onFieldChange} name="secondName" value={this.state.secondName}/></label></p>
                    <p><label>Категория:
                            <select name="category" value={this.state.category} onChange={this.onFieldChange}>
                                <CategoryOption categories={this.state.categories}/>
                            </select>
                        </label></p>
                    <p><label>Логин:<input type="text" onChange={this.onFieldChange} name="login" value={this.state.login}/></label></p>
                    <p><label>Пароль:<input type="password" onChange={this.onFieldChange} name="password" value={this.state.password}/></label></p>
                    <p><button onClick={this.handleSignUp}>Регистрация</button></p>
                </form>
            </div>
        );
    }
}