import React from "react";
import { Alert, Button, Form, Icon, Input, Spin, Typography } from "antd";
import { Link } from "react-router-dom";
import { TokenStorage } from "../../Services/TokenStorage.js";
import { SessionStorage } from "../../Services/SessionStorage.js";

const {Text} = Typography;

const formItemLayout = {
    labelCol: {
        xs: {span: 4},
    },
    wrapperCol: {
        xs: {span: 10},
    },
};

const LoginFormBase = (props, context) => {
    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState(null);
    const {history, loginUser, setRole} = props;

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if (!err) {
                setLoading(true);

                const {username, password} = values;

                fetch("http://localhost:8080/login", {
                    method: 'POST',
                    body: JSON.stringify({
                        username: username,
                        password: password,
                    })
                })
                    .then(response => {
                        console.log(response.status);
                        if (response.status !== 200) {
                            throw new Error("asds");
                        } else {
                            return response.json();
                        }
                    })
                    .then(response => {
                        setLoading(false);
                        loginUser(response.authToken, response.username);
                    })
                    .then(
                        () => fetch(`http://localhost:8080/doctors/find?username=${username}`)
                    )
                    .then(r => r.json())
                    .then(r => {
                        SessionStorage.setRole(r);
                        setRole(r);

                        if (SessionStorage.isPatient(r)) {
                            history.push('/home-user');
                        } else {
                            history.push('/home-doctor');
                        }
                    })
                    .catch(error => {
                        setLoading(false);
                        setError("Error!");
                    })
            }
        });
    };

    const {getFieldDecorator} = props.form;

    if (loading) {
        return <Spin />
    }

    return (
        <>
            {
                error !== null && (
                    <Alert
                        message={error}
                        type="error"
                        closable
                    />
                )
            }
            <Form onSubmit={handleSubmit} {...formItemLayout}>
                <Form.Item label={"Имя пользователя"}>
                    {getFieldDecorator('username', {
                        rules: [{required: true, message: 'Пожалуйста введите имя пользователя!'}],
                    })(
                        <Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}} />}
                               placeholder="Имя пользователя" />
                    )}
                </Form.Item>
                <Form.Item label={"Пароль"}>
                    {getFieldDecorator('password', {
                        rules: [{required: true, message: 'Пожалуйста введите пароль!'}],
                    })(
                        <Input prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}} />} type="password"
                               placeholder="Пароль" />
                    )}
                </Form.Item>
                <Form.Item wrapperCol={{offset: 4, span: 10}}>
                    <Button type="primary" htmlType={"submit"}>
                        Войти
                    </Button>
                    <Text style={{margin: "0 10px"}}>или</Text>
                    <Button type={"dashed"}>
                        <Link to="/signUp">
                            <Icon
                                style={{color: "#32e24e", marginRight: 10}}
                                type={"user-add"}
                            />
                            <Text>Зарегистрироваться</Text>
                        </Link>
                    </Button>
                </Form.Item>
            </Form>

        </>
    )
};

export const LoginForm = Form.create()(LoginFormBase);
