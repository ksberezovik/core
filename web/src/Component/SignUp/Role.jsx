import React from "react";
import {RenderForm} from "./Utils.jsx";

export class RoleCheckbox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isPatient: true,
            isDoctor: false
        };
        this.handleRoleChange = this.handleRoleChange.bind(this);
    }

    handleRoleChange(event) {
        const target = event.target;
        const value = target.checked;
        const name = target.name;

        if (name === 'isDoctor' && value === true) {
            this.setState({
                isDoctor: true,
                isPatient: false
            });
        }

        if (name === 'isPatient' && value === true) {
            this.setState({
                isDoctor: false,
                isPatient: true
            });
        }
    }

    render() {
        return (
            <div>
                <label>
                    Пациент:<input name="isPatient" type="checkbox" checked={this.state.isPatient} onChange={this.handleRoleChange}/>
                    Доктор:<input name="isDoctor" type="checkbox" checked={this.state.isDoctor} onChange={this.handleRoleChange}/>
                </label>

                <RenderForm showPatientForm = {this.state.isPatient} showDoctorForm = {this.state.isDoctor}/>
            </div>
        );
    }
}
