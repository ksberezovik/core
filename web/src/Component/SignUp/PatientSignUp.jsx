import React from "react";
import { Button, Form, Input, message } from "antd";

const formItemLayout = {
    labelCol: {
        xs: {span: 4},
    },
    wrapperCol: {
        xs: {span: 10},
    },
};


class PatientSignUpBase extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '', middleName: '', secondName: '',
            city: '', street: '', house: '', flat: '',
            phone: '', email: '',
            login: '',
            password: ''
        };
        this.onFieldChange = this.onFieldChange.bind(this);
        this.handleSignUp = this.handleSignUp.bind(this);
    };

    onFieldChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        });
    }

    handleSignUp(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                const {
                    firstName,
                    middleName,
                    secondName,
                    city,
                    street,
                    house,
                    flat,
                    phone,
                    email,
                    login,
                    password,

                } = values;

                fetch('http://localhost:8080/users/patient/signup', {
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    body: JSON.stringify({
                        name: {
                            firstName,
                            middleName,
                            secondName,
                        },
                        address: {
                            city,
                            street,
                            house,
                            flat,
                        },
                        contacts: {
                            phone,
                            email,
                        },
                        username: login,
                        password: password,
                    }),
                })
                    .then(request => {
                        if (request.ok) {
                            message.success("Успешная регистрация!")
                        } else {
                            message.error("При регистрация произошла ошибка! Попробуйте позже.")
                        }
                    })

            }
        });
    }

    render() {
        const {form} = this.props;
        const {getFieldDecorator} = form;

        return (
            <div>
                <Form onSubmit={this.handleSignUp} {...formItemLayout}>
                    <Form.Item label={"Фамилия"}>
                        {getFieldDecorator('secondName', {
                            rules: [{required: true, message: 'Пожалуйста введите фамилию!'}],
                        })(
                            <Input placeholder="Иванов" />
                        )}
                    </Form.Item>
                    <Form.Item label={"Имя"}>
                        {getFieldDecorator('firstName', {
                            rules: [{required: true, message: 'Пожалуйста введите имя!'}],
                        })(
                            <Input placeholder="Иван" />
                        )}
                    </Form.Item>

                    <Form.Item label={"Отчество"}>
                        {getFieldDecorator('middleName', {
                            rules: [{required: true, message: 'Пожалуйста введите отчество!'}],
                        })(
                            <Input placeholder="Иванович" />
                        )}
                    </Form.Item>
                    <Form.Item label={"Город"}>
                        {getFieldDecorator('city', {
                            rules: [{required: true, message: 'Пожалуйста введите город!'}],
                        })(
                            <Input placeholder="Минск" />
                        )}
                    </Form.Item>
                    <Form.Item label={"Улица"}>
                        {getFieldDecorator('street', {
                            rules: [{required: true, message: 'Пожалуйста введите улицу!'}],
                        })(
                            <Input placeholder="Ленина" />
                        )}
                    </Form.Item>
                    <Form.Item label={"Дом"}>
                        {getFieldDecorator('house', {
                            rules: [{required: true, message: 'Пожалуйста введите дом!'}],
                        })(
                            <Input placeholder="1" />
                        )}
                    </Form.Item>
                    <Form.Item label={"Квартира"}>
                        {getFieldDecorator('flat', {
                            rules: [{required: true, message: 'Пожалуйста введите номер квартиры!'}],
                        })(
                            <Input placeholder="32" />
                        )}
                    </Form.Item>
                    <Form.Item label={"Телефон"}>
                        {getFieldDecorator('phone', {
                            rules: [{required: true, message: 'Пожалуйста введите номер телефона!'}],
                        })(
                            <Input placeholder="+375291231231" />
                        )}
                    </Form.Item>
                    <Form.Item label={"E-mail"}>
                        {getFieldDecorator('email', {
                            rules: [{required: true, message: 'Пожалуйста введите e-mail!'}],
                        })(
                            <Input placeholder="ivanov@gmail.com" />
                        )}
                    </Form.Item>
                    <Form.Item label={"Логин"}>
                        {getFieldDecorator('login', {
                            rules: [{required: true, message: 'Пожалуйста введите логин!'}],
                        })(
                            <Input placeholder="Логин" />
                        )}
                    </Form.Item>
                    <Form.Item label={"Пароль"}>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: 'Пожалуйста введите пароль!'}],
                        })(
                            <Input type="password" placeholder="Пароль" />
                        )}
                    </Form.Item>
                    <Form.Item wrapperCol={{offset: 4, span: 10}}>
                        <Button type="primary" htmlType={"submit"}>
                            Зарегистрироваться
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

export const PatientSignUp = Form.create()(PatientSignUpBase);
