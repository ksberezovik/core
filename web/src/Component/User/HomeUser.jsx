import React from "react";
import {SessionStorage} from "../../Services/SessionStorage.js";
import {Button, Card, Col, Comment, Row, Spin, Tag} from "antd";
import Avatar from "antd/es/avatar";
import Tooltip from "antd/es/tooltip";
import Icon from "antd/es/icon";
import moment from "moment";
import Text from "antd/es/typography/Text.js";
import {Link} from "react-router-dom";
import Paragraph from "antd/es/skeleton/Paragraph.js";

export class HomeUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            patient: {},
            items: [],
        }
    }

    componentDidMount() {
        this.setState({loading: true});

        const currentUsername =  SessionStorage.getUsername();
        fetch(`http://localhost:8080/doctors/find-patient?username=${currentUsername}`)
            .then(response => {
                return response.json();
            })
            .then(patient => {
                this.setState({loading: false, patient: patient})
            })
            .then(
                () => fetch(`http://localhost:8080/doctors/patient-items?username=${currentUsername}`)
            )
            .then(response => response.json())
            .then(items => {
                this.setState({items: items});
            });
    }

    render() {
        if (this.state.loading) {
            return <Spin size={"large"} />;
        }

        return (
            <div>
                <Avatar size={64} icon="user" />
                <Text>  {this.state.patient.name.firstName} {this.state.patient.name.middleName} {this.state.patient.name.secondName}</Text>
                <br/>
                <br/>
                <Icon type="phone" />
                <Text>   {this.state.patient.contacts.phone}</Text>
                <br/>
                <Icon type="contacts" />
                <Text>   {this.state.patient.contacts.email}</Text>
                <br/>
                <br/>
                <Icon type="environment" />
                <Text>   {this.state.patient.address.city}, {this.state.patient.address.street}, {this.state.patient.address.house} - {this.state.patient.address.flat}</Text>

                <br/>
                <br/>
                <Text>Мои посещения: </Text>
                {
                    this.state.items.length === 0 ? <Text>Нет доступных</Text> : <div/>
                }

                <br/>
                {
                    this.state.items.map((item, index) =>
                        <Row key={index} style={{marginBottom: 24}}>
                            <Col span={10}>
                                <Card title={moment(item.startDate).locale("ru").format('D MMMM')}>

                                    {
                                        item.past
                                            ?   <>
                                            <Row style={{marginBottom: 5}}>
                                                <Col span={12}>
                                                    Результат приёма:
                                                </Col>
                                                <Col span={12} style={{textAlign: "right"}}>
                                                    {item.result}
                                                </Col>
                                            </Row>
                                            <Row style={{marginBottom: 5}}>
                                                <Col span={12}>
                                                    Назначенные процедуры:
                                                </Col>
                                                <Col span={12} style={{textAlign: "right"}}>
                                                    {item.remark}
                                                </Col>
                                            </Row>
                                                <Row style={{marginBottom: 5}}>
                                                    <Col span={12} >
                                                        Статус:
                                                    </Col>
                                                    <Col span={12} style={{textAlign: "right"}}>
                                                        <Tag color="red">ЗАВЕРШЕН</Tag>
                                                    </Col>
                                                </Row>
                                                </>

                                            :
                                            <>
                                                <Row style={{marginBottom: 5}}>
                                                    <Col span={12}>
                                                        Время посещения:
                                                    </Col>
                                                    <Col span={12} style={{textAlign: "right"}}>
                                                        {moment(item.startDate).locale("ru").format('HH:mm')} - {moment(item.endDate).locale("ru").format('HH:mm')}
                                                    </Col>
                                                </Row>
                                                <Row style={{marginBottom: 5}}>
                                                    <Col span={12} >
                                                        Статус:
                                                    </Col>
                                                    <Col span={12} style={{textAlign: "right"}}>
                                                        <Tag color="blue">ОЖИДАЕТСЯ</Tag>
                                                    </Col>
                                                </Row>
                                            </>

                                    }
                                    <Row gutter={24}>
                                    </Row>
                                </Card>
                            </Col>
                        </Row>)
                }
            </div>

        );
    }
}