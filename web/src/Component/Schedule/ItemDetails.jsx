import React from "react";
import {Link} from "react-router-dom";
import {Button, Card, Col, Row, Spin} from "antd";
import {SessionStorage} from "../../Services/SessionStorage.js";

export class ItemDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            schedules: [],
        }
    }

    componentDidMount() {

        this.setState({loading: true});

        const currentUsername =  SessionStorage.getUsername();
        fetch(`http://localhost:8080/doctors/doctorSchedule?username=${currentUsername}`)
            .then(response => {
                return response.json();
            })
            .then(schedules => {
                this.setState({loading:false, schedules: schedules})
            });
    }

    render() {
        if (this.state.loading) {
            return <Spin size={"large"} />;
        }

        return <>
            {
                this.state.schedules.map((schedule, index) => <Row key={index}>
                    <p>{schedule.day}</p>
                    <Button type={"primary"}>
                        <Link to={`/item/1`}>
                            Детали
                        </Link>
                    </Button>
                </Row>)
            }
        </>
    }
}