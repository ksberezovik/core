import React from "react";
import { Button, Icon, Radio, Row, Select, Spin, message, Tag, Typography, Tooltip } from "antd";
import moment from "moment";
import { SessionStorage } from "../../Services/SessionStorage.js";

const {Text} = Typography;
const Option = Select.Option;

const dateFormat = 'HH:mm';

export class Schedule extends React.Component {
    constructor(props) {
        super(props);

        const {match: {params: {doctorId}}} = this.props;

        this.state = {
            loading: false,
            selectedItemKey: undefined,
            selectedDate: undefined,
            items: [],
            doctorId,
            date: undefined,
        };

        this.selectDay = this.selectDay.bind(this);
        this.handleSelectDay = this.handleSelectDay.bind(this);
        this.saveSelectedItem = this.saveSelectedItem.bind(this);
        this.loadData = this.loadData.bind(this);
    }

    componentDidMount() {
        const {doctorId} = this.state;

        this.loadData(doctorId);
    }

    loadData(doctorId) {
        this.setState({loading: true});
        fetch(`http://localhost:8080/doctors/schedule?doctorId=${doctorId}`)
            .then(r => r.json())
            .then(data => {
                this.setState({loading: false, items: data})
            });
    }

    selectDay(e) {
        this.setState({
            selectedItemKey: e.target.value,
        })
    }

    handleSelectDay(selectedDateKey) {
        const {items, selectedItemKey, doctorId} = this.state;

        const date = items[selectedItemKey].availableItems[selectedDateKey];
        this.setState({date: date});
    }

    saveSelectedItem() {
        const {doctorId, date} = this.state;

        fetch('http://192.168.0.3:8080/doctors/addItem', {
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                start: date.start,
                end: date.end,
                username: SessionStorage.getUsername(),
                doctorId: doctorId
            }),
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error("asds");
                }

                message.success("Запись успешна!");

                this.setState({
                    selectedItemKey: undefined,
                    selectedDate: undefined,
                    date: undefined,
                })
            })
            .then(() => this.loadData(doctorId))
            .catch(() => {
                message.error("Произошла ошибка. Попробуйте позже.")
            })
    }

    render() {
        const {loading, items, selectedItemKey, date} = this.state;

        if (loading) {
            return <Spin />;
        }

        return (
            <div>
                <Row>
                    <Radio.Group buttonStyle="solid">
                        {
                            items.map((v, k) => {
                                const freeCount = v.availableItems
                                    .filter(aItem => aItem.free)
                                    .reduce((acc) => {
                                        acc += 1;
                                        return acc;
                                    }, 0);

                                return (
                                    <Radio.Button
                                        key={k}
                                        value={k}
                                        onChange={(v) => this.selectDay(v)}
                                    >
                                        <Text
                                            style={{marginRight: 14}}
                                            strong
                                        >
                                            {moment(v.day).locale("ru").format('D MMMM')}
                                        </Text>
                                        <Tooltip title={"Доступно для записи"}>
                                        <Tag color="blue">{freeCount}</Tag>
                                        </Tooltip>
                                    </Radio.Button>
                                )
                            })
                        }
                    </Radio.Group>
                </Row>
                {
                    selectedItemKey !== undefined && (
                        <Row style={{marginTop: 20}}>
                            <Select
                                onChange={this.handleSelectDay}
                                style={{width: 400}}
                            >
                                {
                                    items[selectedItemKey].availableItems.map((v, i) => (
                                        <Option
                                            key={i}
                                            disabled={!v.free}
                                            value={i}
                                        >
                                            {`${moment(v.start).format(dateFormat)} - ${moment(v.end).format(dateFormat)}`}
                                        </Option>
                                    ))
                                }
                            </Select>
                        </Row>
                    )
                }
                {
                    date !== undefined && (
                        <Button
                            type={"primary"}
                            style={{marginTop: 20}}
                            onClick={this.saveSelectedItem}
                        >
                            <Icon type={"carry-out"} />
                            Записаться
                        </Button>
                    )
                }
            </div>

        );
    }

}
