import React from "react";
import { Link } from "react-router-dom";
import { Breadcrumb, Button, Col, Divider, Icon, Row, Spin, Typography } from "antd";
import { DoctroImage } from "../Doctor/DoctorImage.jsx";

const {Text} = Typography;

export class Category extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            doctors: [],
        }
    }

    componentDidMount() {
        const {match: {params: {id}}} = this.props;

        this.setState({loading: true});
        fetch(`http://localhost:8080/doctors/by-category?categoryId=${id}`)
            .then(r => r.json())
            .then(doctors => {
                this.setState({loading: false, doctors})
            })
    }

    render() {
        const {loading, doctors} = this.state;

        if (loading) {
            return <Spin size={"large"} />;
        }

        return (
            <>
                <Breadcrumb style={{marginBottom: 20}}>
                    <Breadcrumb.Item><Link to="/">Главная</Link></Breadcrumb.Item>
                    <Breadcrumb.Item><Link to="/categories">Специализации</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>Врачи</Breadcrumb.Item>
                </Breadcrumb>
                {
                    doctors.map((doctor, i) => (
                            <div key={i}>
                                <Row>
                                    <Col span={4}>
                                        <DoctroImage imgPath={doctor.imgPath} />
                                    </Col>
                                    <Col>
                                        <p style={{height: 135, overflow: "hidden"}}>{doctor.doctorInfo}</p>
                                    </Col>
                                    <Row style={{textAlign: "right"}}>
                                        <Button type={"ghost"}>
                                            <Link to={`/doctor/${doctor.id}`}>
                                                <Icon type={"idcard"} style={{marginRight: 10}} />
                                                <Text strong>{`${doctor.name.firstName} ${doctor.name.secondName}`}</Text>
                                            </Link>
                                        </Button>
                                    </Row>
                                </Row>
                                <Divider />
                            </div>
                        )
                    )
                }
            </>

        );
    }
}
