import React from "react";
import {Link} from "react-router-dom";
import {Breadcrumb, Button, Card, Col, Row, Spin, Typography} from "antd";


const {Text} = Typography;

export class CategoryList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            categories: [],
            error: null,
        };
    }

    componentDidMount() {
        fetch("http://localhost:8080/categories/all")
            .then(response => response.json())
            .then(categories => this.setState({loading: false, categories: categories}))
            .catch(err => this.setState({loading: false, error: "Server error!"}))
    }

    render() {
        if (this.state.loading) {
            return <Spin size={"large"}/>;
        }

        if (this.state.error !== null) {
            return <span>Error: {this.state.error}</span>
        }


        return (
            <>
                <Breadcrumb>
                    <Breadcrumb.Item><Link to="/">Главная</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>Специализации</Breadcrumb.Item>
                </Breadcrumb>
                <Row gutter={24} style={{marginTop: 24}}>
                    {
                        this.state.categories.map((category, index) =>
                            <Col span={12} key={index} style={{marginBottom: 24}} >
                                <Card
                                    title={category.name}
                                    style={{height: 320, overflow: "hidden"}}
                                    extra={<Button type={"primary"}>
                                        <Link to={`/category/${category.id}`}>
                                            Просмотреть врачей
                                        </Link>
                                    </Button>}
                                >
                                    <Text>{category.description}</Text>
                                    <br/>
                                    <br/>

                                </Card>
                            </Col>
                        )
                    }
                </Row>
            </>
        );
    }
}
