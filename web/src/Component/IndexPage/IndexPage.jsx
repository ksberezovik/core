import React from "react";
import { Typography } from "antd";
import IndexImage from "../../Assets/img/index.jpg";

const {Title} = Typography;

const imageStyle = {
    width: "100%",
    height: "400px",
};

export const IndexPage = (props) => {

    return (
        <>
            <Title>Приложение для управления медицинским центром.</Title>
            <img
                style={imageStyle}
                src={IndexImage}
            />

        </>
    )
};
