import React from "react";
import { Link } from "react-router-dom";
import { Breadcrumb, Button, Col, Icon, Row, Spin, Typography } from "antd";
import { DoctroImage } from "./DoctorImage.jsx";

const {Text, Title} = Typography;

export class DoctorDetail extends React.Component {
    constructor(props) {
        super(props);

        const {match: {params: {doctorId}}} = this.props;

        this.state = {
            loading: true,
            doctor: {},
            doctorId,
        }
    }

    componentDidMount() {
        const {doctorId} = this.state;

        this.setState({loading: true});
        fetch(`http://localhost:8080/doctors/doctor?doctorId=${doctorId}`)
            .then(response => {
                return response.json();
            })
            .then(response => {
                this.setState({loading: false, doctor: response.doctor});
            });
    }

    render() {
        const {loading, doctor} = this.state;

        if (loading) {
            return <Spin size={"large"} />;
        }

        const {id, doctorInfo, imgPath, name} = doctor;

        const fullName = `${name.secondName} ${name.firstName} ${name.middleName}`;

        return (
            <>
                <Breadcrumb style={{margin: '16px 0'}}>
                    <Breadcrumb.Item><Link to="/">Главная</Link></Breadcrumb.Item>
                    <Breadcrumb.Item><Link to={`/doctors/all`}>Врачи</Link></Breadcrumb.Item>
                    <Breadcrumb.Item>{fullName}</Breadcrumb.Item>
                </Breadcrumb>
                <Row>
                    <Title level={3}>{fullName}</Title>
                </Row>
                <Row>
                    <Col span={4}>
                        <DoctroImage imgPath={imgPath} />
                    </Col>
                    <Col>
                        <Text>
                            {doctorInfo}
                        </Text>
                    </Col>
                </Row>
                <Row style={{marginTop: 20}}>
                    <Button type={"primary"} block>
                        <Link to={`/schedule/${id}`}>
                            <Icon
                                type={"calendar"}
                                style={{marginRight: 10}}
                            />
                            Расписание
                        </Link>
                    </Button>

                </Row>
            </>
        );
    }
}
