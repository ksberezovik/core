import React from "react";
import DefaultDoctorImage from "./../../Assets/img/doctors/default-path.jpg";
import Doctor1Image from "./../../Assets/img/doctors/path1.jpg";
import Doctor2Image from "./../../Assets/img/doctors/path2.jpg";
import Doctor3Image from "./../../Assets/img/doctors/path3.jpg";
import Doctor4Image from "./../../Assets/img/doctors/path4.jpg";


const getDoctorImage = (path) => {
  switch (path) {
    case "path1":
      return Doctor1Image;
    case "path2":
      return Doctor2Image;
    case "path3":
      return Doctor3Image;
    case "path4":
      return Doctor4Image;
    default:
      return DefaultDoctorImage;
  }
};

export const DoctroImage = (props) => {
  const {imgPath} = props;

  return <img
    style={{width: 140, height: 180}}
    src={getDoctorImage(imgPath)}
  />
};
