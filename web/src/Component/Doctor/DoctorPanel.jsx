import React from "react";
import {Link} from "react-router-dom";
import {Button, Card, Col, Row, Spin, Typography} from "antd";
import moment from "moment";
import {SessionStorage} from "../../Services/SessionStorage.js";
import {TokenStorage} from "../../Services/TokenStorage.js";

const {Text, Paragraph} = Typography;

export class DoctorPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            schedules: [],
        }
    }

    componentDidMount() {

        this.setState({loading: true});

        const currentUsername = SessionStorage.getUsername();
        fetch(`http://localhost:8080/doctors/doctorSchedule?username=${currentUsername}`)
            .then(response => {
                return response.json();
            })
            .then(schedules => {
                this.setState({loading: false, schedules: schedules})
            });
    }

    render() {
        if (this.state.loading) {
            return <Spin size={"large"}/>;
        }

        return <>
            {
                this.state.schedules.map((schedule, index) =>
                    <Row key={index} style={{marginBottom: 24}}>
                        <Col span={24}>
                            <Card title={moment(schedule.day).locale("ru").format('D MMMM')}>
                                <Paragraph>
                                    <Text style={{marginRight: 10}}>Количество записей:</Text>
                                    <Text strong>{schedule.filledItems.length}</Text>
                                </Paragraph>
                                <Row gutter={24}>
                                    {
                                        this.state.schedules[index].filledItems.map((item, itemIndex) => (
                                                <Col span={2} key={itemIndex}>
                                                    {
                                                        item.past
                                                            ? <Button type="dashed">
                                                                <Link to={`/doctor-item-details/${item.itemIndex}`}>
                                                                    {moment(item.start).format('HH:mm')}
                                                                </Link>
                                                            </Button>
                                                            : <Button type={"primary"}>
                                                                <Link to={`/doctor-item-details/${item.itemIndex}`}>
                                                                    {moment(item.start).format('HH:mm')}
                                                                </Link>
                                                            </Button>
                                                    }
                                                </Col>
                                            )
                                        )
                                    }
                                </Row>
                            </Card>
                        </Col>
                    </Row>)
            }
        </>
    }
}
