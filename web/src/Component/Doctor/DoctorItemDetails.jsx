import React from "react";
import {Link} from "react-router-dom";
import {Button, Card, Col, Form, Icon, Input, Row, Spin, Typography} from "antd";
import moment from "moment";
import {SessionStorage} from "../../Services/SessionStorage.js";


const formItemLayout = {
    labelCol: {
        xs: {span: 2},
    },
    wrapperCol: {
        xs: {span: 8},
    },
};


const {Text} = Typography;

export class DoctorItemDetailsBase extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            item: {},
            result: "",
            procedure: "",
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        const {match: {params: {id}}, form} = this.props;
        const {getFieldDecorator} = form;


        this.setState({loading: true});
        fetch(`http://localhost:8080/doctors/itemDetails?itemId=${id}`)
            .then(response => {
                return response.json();
            })
            .then(item => {
                this.setState({loading: false, item: item})
            });
    }

    render() {
        if (this.state.loading) {
            return <Spin size={"large"} />;
        }


        return (
            <>
                <div>
                Пациент:
                <p>{this.state.item.patient.name.firstName} {this.state.item.patient.name.secondName}</p>
                Дата приёма:
                <p>{moment(this.state.item.start).format('MMMM DD')}</p>
                Время приёма:
                <p>{moment(this.state.item.startDate).format('HH:mm')} - {moment(this.state.item.endDate).format('HH:mm')}</p>
                </div>

                {
                    this.state.item.past ?
                        <div>
                            <Text>Результат приёма: </Text>
                            <Text>{this.state.item.result}</Text>
                            <br/>
                            <br/>
                            <Text>Назначенные процедуры: </Text>
                            <Text>{this.state.item.remark}</Text>
                        </div> :
                        <div>
                            <Text>Результат приёма: </Text>
                            <br />
                            <textarea
                                onChange={(e) => {this.setState({result: e.target.value})}}
                                style={{width: 500, height: 150}}/>
                            <br/>
                            <Text>Назначенные процедуры: </Text>
                            <br/>
                            <textarea
                                onChange={(e) => {this.setState({procedure: e.target.value})}}
                                style={{width: 500, height: 75}}
                            />
                            <br/>
                            <Button
                                type={"primary"}
                                onClick={this.handleSubmit}>
                                Завершить приём
                            </Button>
                        </div>
                }
            </>
        );
    }

    handleSubmit() {

        const {history} = this.props;


        fetch("http://localhost:8080/doctors/close-item", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                itemId: this.state.item.id,
                result: this.state.result,
                procedure: this.state.procedure,
            })
        }).then(response => {
                history.push('/home-doctor')
            })
    }
}

export const DoctorItemDetails = Form.create()(DoctorItemDetailsBase);