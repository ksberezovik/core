import {TokenStorage} from "./TokenStorage.js";

export class SessionStorage {

    static setUsername(username) {
        localStorage.setItem(SessionStorage.SESSION_KEY, username);
    }

    static getUsername() {
        return localStorage.getItem(SessionStorage.SESSION_KEY);
    }

    static setRole(role) {
        localStorage.setItem(SessionStorage.USER_ROLE, role);
    }

    static getRole() {
        return localStorage.getItem(SessionStorage.USER_ROLE);
    }

    static cleanRole() {
        localStorage.removeItem(SessionStorage.USER_ROLE)
    }

    static isPatient(role) {
        return role === 'PATIENT';
    }

    static isDoctor(role) {
        return role === 'DOCTOR';
    }

    static cleanUsername() {
        localStorage.removeItem(SessionStorage.SESSION_KEY)
    }

    static isLoggedIn() {
        return TokenStorage.getToken() !== null;
    }

    static logOut() {
        TokenStorage.cleanToken();
        this.cleanUsername();
        this.cleanRole();
    }
}

SessionStorage.SESSION_KEY = 'session';
SessionStorage.USER_ROLE = 'role';