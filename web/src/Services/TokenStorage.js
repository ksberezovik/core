export class TokenStorage {
    /**
     * @param token string
     */
    static setToken(token) {
        localStorage.setItem(TokenStorage.TOKEN_KEY, token);
    }

    static getToken() {
        return localStorage.getItem(TokenStorage.TOKEN_KEY);
    }

    static cleanToken() {
        localStorage.removeItem(TokenStorage.TOKEN_KEY)
    }
}

TokenStorage.TOKEN_KEY = 'jwtToken';