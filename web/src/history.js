import createHistory from 'history/createBrowserHistory';

const historyInstance = createHistory();

export const history = historyInstance ;