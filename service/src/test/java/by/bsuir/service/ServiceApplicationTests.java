package by.bsuir.service;

import by.bsuir.service.service.schedule.ItemDto;
import by.bsuir.service.service.schedule.ScheduleDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void createScheduleFillDays() {

        List<Integer> DAYS_OF_WORK = Arrays.asList(Calendar.MONDAY,
                Calendar.TUESDAY,
                Calendar.WEDNESDAY,
                Calendar.THURSDAY,
                Calendar.FRIDAY);
        List<ScheduleDto> schedulesDto = new ArrayList<>();

        Calendar now = Calendar.getInstance();
        int nowDayOfWeek = now.get(Calendar.DAY_OF_WEEK);
        for(int i = 0, j = nowDayOfWeek; schedulesDto.size() < 7; i++, j++) {
            int dayOfWeek = j % 7;
            if (DAYS_OF_WORK.contains(dayOfWeek)) {
                ScheduleDto scheduleDto = new ScheduleDto();
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, i);
                scheduleDto.setDay(calendar);
                schedulesDto.add(scheduleDto);
            }
        }

        for (ScheduleDto scheduleDto : schedulesDto) {
            Calendar start = Calendar.getInstance();
            start.setTime(scheduleDto.getDay().getTime());
            start.set(Calendar.HOUR_OF_DAY, 9);
            start.set(Calendar.MINUTE, 0);

            Calendar end = Calendar.getInstance();
            end.set(Calendar.HOUR_OF_DAY, 16);
            end.set(Calendar.MINUTE, 0);

            while (start.before(end)) {
                ItemDto itemDto = new ItemDto();
                itemDto.setStart(start);
                Calendar timeEnd = Calendar.getInstance();
                timeEnd.setTime(start.getTime());
                timeEnd.add(Calendar.MINUTE, 30);
                start = Calendar.getInstance();
                start.setTime(timeEnd.getTime());
                itemDto.setEnd(timeEnd);
                scheduleDto.getAvailableItems().add(itemDto);
            }
        }
    }

}
