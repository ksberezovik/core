package by.bsuir.service.service.schedule;

import lombok.*;

import java.util.Calendar;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReceivedItemDto {
    private Calendar start;
    private Calendar end;

    private String username;

    private Long doctorId;
}
