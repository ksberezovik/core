package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.entity.user.Doctor;
import by.bsuir.dao.persistent.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    public Schedule findByDoctor(Doctor doctor) {
        return scheduleRepository.findByDoctor(doctor);
    }
}
