package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.user.Patient;

public interface PatientService {
    void save(Patient patient);
    Patient findByUsername(String username);
}
