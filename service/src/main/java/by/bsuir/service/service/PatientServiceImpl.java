package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.user.Patient;
import by.bsuir.dao.persistent.entity.user.Role;
import by.bsuir.dao.persistent.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientRepository patientRepository;

    public void save(Patient patient) {
        patient.setRole(Role.PATIENT);
        patientRepository.save(patient);
    }

    @Override
    public Patient findByUsername(String username) {
        return patientRepository.findByUsername(username);
    }
}
