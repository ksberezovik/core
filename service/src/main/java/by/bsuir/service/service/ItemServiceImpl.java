package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.schedule.Item;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.entity.user.Doctor;
import by.bsuir.dao.persistent.entity.user.Patient;
import by.bsuir.dao.persistent.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    public void save(Item item) {
        itemRepository.save(item);
    }

    @Override
    public List<Item> findBySchedule(Schedule schedule) {
        return itemRepository.findBySchedule(schedule);
    }

    @Override
    public void delete(Item item) {
        itemRepository.delete(item);
    }

    @Override
    public Item findById(Long id) {
        return itemRepository.findById(id).get();
    }

    @Override
    public void removeById(Long id) {
        itemRepository.removeById(id);
    }

    @Override
    public void addItem(Schedule schedule, Patient patient, Calendar start, Calendar end) {
        Item item = new Item(start.getTime(), end.getTime(), patient, schedule, "test", "", false);
        itemRepository.save(item);
    }

    @Override
    public List<Item> findByPatient(Patient patient) {
        return itemRepository.findByPatient(patient);
    }
}
