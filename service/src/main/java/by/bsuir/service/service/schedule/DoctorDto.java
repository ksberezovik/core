package by.bsuir.service.service.schedule;

import by.bsuir.dao.persistent.entity.user.Doctor;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class DoctorDto {
    private Doctor doctor;
    private String categoryId;
}
