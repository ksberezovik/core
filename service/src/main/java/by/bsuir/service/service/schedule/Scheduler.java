package by.bsuir.service.service.schedule;

import by.bsuir.dao.persistent.entity.schedule.Item;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.entity.user.Doctor;

import java.util.*;

public class Scheduler {

    public static int DURATION_IN_MINUTES = 30;
    public static List<Integer> DAYS_OF_WORK = Arrays.asList(Calendar.MONDAY,
                                                                Calendar.TUESDAY,
                                                                Calendar.WEDNESDAY,
                                                                Calendar.THURSDAY,
                                                                Calendar.FEBRUARY);
    public static int START_HOURS = 9;
    public static int END_HOURS = 16;


    public static List<ScheduleDto> getSchedulesDto(Schedule schedule) {
        List<ScheduleDto> availableSchedules = createScheduleAndFillDays();
        fillTimes(availableSchedules);
        markFree(schedule, availableSchedules);
        return availableSchedules;
    }

    public static List<ScheduleDto> getDoctorSchedulesDto(Schedule schedule) {
        List<ScheduleDto> availableSchedules = createDoctorScheduleAndFillDays();
        writeFilledItems(availableSchedules, schedule);
        return availableSchedules;
    }

    private static List<ScheduleDto> createScheduleAndFillDays() {
        List<ScheduleDto> schedulesDto = new ArrayList<>();

        Calendar now = Calendar.getInstance();
        int nowDayOfWeek = now.get(Calendar.DAY_OF_WEEK);
        for(int i = 0, j = nowDayOfWeek; schedulesDto.size() < 7; i++, j++) {
            int dayOfWeek = j % 7;
            if (DAYS_OF_WORK.contains(dayOfWeek)) {
                ScheduleDto scheduleDto = new ScheduleDto();
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, i);
                scheduleDto.setDay(calendar);
                schedulesDto.add(scheduleDto);
            }
        }
        return schedulesDto;
    }

    private static List<ScheduleDto> createDoctorScheduleAndFillDays() {
        List<ScheduleDto> schedulesDto = new ArrayList<>();

        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, -7);
        int nowDayOfWeek = now.get(Calendar.DAY_OF_WEEK);
        for(int i = 0, j = nowDayOfWeek; schedulesDto.size() < 14; i++, j++) {
            int dayOfWeek = j % 7;
            if (DAYS_OF_WORK.contains(dayOfWeek)) {
                ScheduleDto scheduleDto = new ScheduleDto();
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, i);
                scheduleDto.setDay(calendar);
                schedulesDto.add(scheduleDto);
            }
        }
        return schedulesDto;
    }

    private static void fillTimes(List<ScheduleDto> schedulesDto) {
        for (ScheduleDto scheduleDto : schedulesDto) {
            Calendar start = Calendar.getInstance();
            start.setTime(scheduleDto.getDay().getTime());
            start.set(Calendar.HOUR_OF_DAY, START_HOURS);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);
            start.set(Calendar.MILLISECOND, 0);

            Calendar end = Calendar.getInstance();
            end.setTime(start.getTime());
            end.set(Calendar.HOUR_OF_DAY, END_HOURS);
            end.set(Calendar.MINUTE, 0);

            while (start.before(end)) {
                ItemDto itemDto = new ItemDto();
                itemDto.setStart(start);
                Calendar timeEnd = Calendar.getInstance();
                timeEnd.setTime(start.getTime());
                timeEnd.add(Calendar.MINUTE, DURATION_IN_MINUTES);
                start = Calendar.getInstance();
                start.setTime(timeEnd.getTime());
                itemDto.setEnd(timeEnd);
                scheduleDto.getAvailableItems().add(itemDto);
            }
        }
    }

    private static void markFree(Schedule schedule, List<ScheduleDto> schedulesDto) {
        for (Item item : schedule.getItems()) {
            Date start = new Date(item.getStartDate().getTime()) ;
            Calendar itemStart = Calendar.getInstance();
            itemStart.setTime(start);
            itemStart.set(Calendar.SECOND, 0);
            itemStart.set(Calendar.MILLISECOND, 0);

            for (ScheduleDto scheduleItem : schedulesDto) {
                for (ItemDto itemDto : scheduleItem.getAvailableItems()) {
                    itemDto.getStart().set(Calendar.SECOND, 0);
                    itemDto.getStart().set(Calendar.MILLISECOND, 0);
                    if (itemStart.compareTo(itemDto.getStart()) == 0) {
                        itemDto.setFree(false);
                    }
                }
            }
        }
    }

    public static void writeFilledItems(List<ScheduleDto> scheduleDtos, Schedule schedule) {
        for (ScheduleDto scheduleDto : scheduleDtos) {
            for (Item item : schedule.getItems()) {
                Calendar itemStartDate = Calendar.getInstance();
                itemStartDate.setTime(item.getStartDate());
                if (scheduleDto.getDay().get(Calendar.DAY_OF_YEAR) == itemStartDate.get(Calendar.DAY_OF_YEAR)) {
                    ItemDto itemDto = new ItemDto();
                    Calendar start = Calendar.getInstance();
                    start.setTime(item.getStartDate());
                    itemDto.setStart(start);

                    Calendar end = Calendar.getInstance();
                    end.setTime(item.getEndDate());
                    itemDto.setEnd(end);

                    itemDto.setPatientName(item.getPatient().getName().getFullName());
                    itemDto.setItemIndex(item.getId().toString());
                    itemDto.setPast(item.isPast());
                    itemDto.setResult(item.getResult());
                    itemDto.setRemark(item.getRemark());
                    scheduleDto.getFilledItems().add(itemDto);
                }
            }
        }
    }
}