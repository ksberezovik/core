package by.bsuir.service.service.schedule;

import lombok.*;

import java.util.Calendar;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class ItemDto {
    private Calendar start;
    private Calendar end;

    private boolean isFree = true;

    private String patientName;
    private String itemIndex;
    private boolean isPast;
    private String result;
    private String remark;
}
