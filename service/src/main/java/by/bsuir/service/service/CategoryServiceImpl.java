package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.doctor.Category;
import by.bsuir.dao.persistent.repository.CategoryRepository;
import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> findAll() {
        List<Category> categories = Lists.newArrayList(categoryRepository.findAll());
        return categories;
    }

    @Override
    public Category findById(Long id) {
        return categoryRepository.findById(id).get();
    }
}