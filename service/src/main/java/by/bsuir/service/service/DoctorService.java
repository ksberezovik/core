package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.user.Doctor;

import java.util.List;

public interface DoctorService {
    void save(Doctor doctor);
    List<Doctor> getDoctors();
    List<Doctor> getDoctorsByCategory(Long categoryId) throws IllegalAccessException;
    Doctor findById(Long id);
    Doctor findByUsername(String username);
}
