package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.user.ApplicationUser;

public interface UserService {
    ApplicationUser findByUsername(String username);
    void saveUser(ApplicationUser applicationUser);
}