package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.doctor.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category findById(Long id);
}
