package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.doctor.Category;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.entity.user.Doctor;
import by.bsuir.dao.persistent.entity.user.Role;
import by.bsuir.dao.persistent.repository.CategoryRepository;
import by.bsuir.dao.persistent.repository.DoctorRepository;
import by.bsuir.dao.persistent.repository.ScheduleRepository;
import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    DoctorRepository doctorRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    public void save(Doctor doctor) {
        String categoryName = doctor.getCategory().getName();
        Category category = categoryRepository.findByName(categoryName);
        doctor.setCategory(category);
        doctor.setRole(Role.DOCTOR);
        doctorRepository.save(doctor);
        Schedule schedule = new Schedule();
        schedule.setDoctor(doctor);
        scheduleRepository.save(schedule);
    }

    public List<Doctor> getDoctors() {
        List<Doctor> doctors = Lists.newArrayList(doctorRepository.findAll());
        return doctors;
    }

    public List<Doctor> getDoctorsByCategory(Long categoryId) throws IllegalAccessException {
        Optional<Category> category = categoryRepository.findById(categoryId);
        if (category.isEmpty()) {
            throw new IllegalAccessException("category with id " + categoryId + " not found.");
        }
        Set<Doctor> doctors = doctorRepository.findByCategory(category.get());
        return new ArrayList<>(doctors);
    }

    @Override
    public Doctor findById(Long id) {
        return doctorRepository.findById(id).get();
    }

    @Override
    public Doctor findByUsername(String username) {
        return doctorRepository.findByUsername(username);
    }
}