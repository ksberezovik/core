package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.user.ApplicationUser;
import by.bsuir.dao.persistent.entity.user.Patient;
import by.bsuir.dao.persistent.entity.user.Role;
import by.bsuir.dao.persistent.repository.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Override
    public ApplicationUser findByUsername(String username) {
        if (username == null) {
            throw new IllegalStateException("required username");
        }
        ApplicationUser applicationUser = applicationUserRepository.findByUsername(username);
        if (applicationUser == null) {
            throw new IllegalStateException("user with name " + username + " not found");
        }
        return applicationUser;
    }

    @Override
    public void saveUser(ApplicationUser applicationUser) {
        applicationUser.setRole(applicationUser.getSystemRole());
        applicationUserRepository.save(applicationUser);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser applicationUser = findByUsername(username);
        return new User(applicationUser.getUsername(), applicationUser.getPassword(), getUserAuthorities(applicationUser));
    }

    private Set<GrantedAuthority> getUserAuthorities(ApplicationUser applicationUser) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(applicationUser.getRole().name()));
        return grantedAuthorities;
    }
}
