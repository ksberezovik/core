package by.bsuir.service.service.schedule;

import lombok.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class ScheduleDto {
    private Calendar day;
    private List<ItemDto> availableItems = new ArrayList<>();

    private List<ItemDto> filledItems = new ArrayList<>();
}
