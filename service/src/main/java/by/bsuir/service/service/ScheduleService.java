package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.entity.user.Doctor;


public interface ScheduleService {
    Schedule findByDoctor(Doctor doctor);
}
