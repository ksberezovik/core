package by.bsuir.service.service;

import by.bsuir.dao.persistent.entity.schedule.Item;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.entity.user.Doctor;
import by.bsuir.dao.persistent.entity.user.Patient;

import java.util.Calendar;
import java.util.List;

public interface ItemService {
    void save(Item item);
    List<Item> findBySchedule(Schedule schedule);
    void delete(Item item);
    Item findById(Long id);
    void removeById(Long id);
    void addItem(Schedule schedule, Patient patient, Calendar start, Calendar end);
    List<Item> findByPatient(Patient patient);
}
