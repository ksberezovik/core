package by.bsuir.service.service.schedule;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class DoctorItemDto {
    private Long itemId;
    private String result;
    private String procedure;
}
