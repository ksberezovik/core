package by.bsuir.service.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class SessionDto {
    private String authToken;
    private String username;
}
