package by.bsuir.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"by.bsuir.dao", "by.bsuir.service"})
public class ServiceConfig {
}
