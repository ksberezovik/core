package by.bsuir.service.config;

import by.bsuir.service.endpoint.CategoryEndpoint;
import by.bsuir.service.endpoint.DoctorEndpoint;
import by.bsuir.service.endpoint.UserEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(DoctorEndpoint.class);
        register(UserEndpoint.class);
        register(CategoryEndpoint.class);
        register(new CORSFilter());
    }
}