package by.bsuir.service.endpoint;

import by.bsuir.dao.persistent.entity.schedule.Item;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.entity.user.ApplicationUser;
import by.bsuir.dao.persistent.entity.user.Doctor;
import by.bsuir.dao.persistent.entity.user.Patient;
import by.bsuir.dao.persistent.repository.ApplicationUserRepository;
import by.bsuir.service.service.*;
import by.bsuir.service.service.schedule.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import java.util.List;

@Component
@Produces("application/json")
@Path("/doctors")
public class DoctorEndpoint {

    @Autowired
    DoctorService doctorService;

    @Autowired
    ScheduleService scheduleService;

    @Autowired
    ItemService itemService;

    @Autowired
    private UserService userService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/by-category")
    public String getDoctorsByCategory(@QueryParam("categoryId") String id) throws JsonProcessingException, IllegalAccessException {
        long categoryId;
        try {
            categoryId = Long.parseLong(id);
        } catch (Exception e) {
            categoryId = 1L;
        }

        List<Doctor> doctors = doctorService.getDoctorsByCategory(categoryId);
        return mapper.writeValueAsString(doctors);
    }

    @GET
    @Path("/all")
    public String getDoctors() throws JsonProcessingException {
        Iterable<Doctor> doctors = doctorService.getDoctors();
        return mapper.writeValueAsString(doctors);
    }

    @GET
    @Path("/schedule")
    public String getScheduleByDoctor(@QueryParam("doctorId") String doctorId) throws JsonProcessingException {
        Doctor doctor = doctorService.findById(Long.parseLong(doctorId));
        Schedule schedule = scheduleService.findByDoctor(doctor);
        List<ScheduleDto> schedulesDto = Scheduler.getSchedulesDto(schedule);
        return mapper.writeValueAsString(schedulesDto);
    }

    @GET
    @Path("/doctor")
    public String getDoctorById(@QueryParam("doctorId") String doctorId) throws JsonProcessingException {
        Doctor doctor = doctorService.findById(Long.parseLong(doctorId));
        return mapper.writeValueAsString(new DoctorDto(doctor, doctor.getCategory().getId().toString()));
    }

    @GET
    @Path("/find")
    public String getByUsername(@QueryParam("username") String username) throws JsonProcessingException {
        ApplicationUser user = applicationUserRepository.findByUsername(username);
        return mapper.writeValueAsString(user.getRole());
    }

    @POST
    @Path("/addItem")
    public void addItem(ReceivedItemDto item) {
        String userName = item.getUsername();
        Patient patient = patientService.findByUsername(userName);
        Doctor doctor = doctorService.findById(item.getDoctorId());
        Schedule schedule = scheduleService.findByDoctor(doctor);
        itemService.addItem(schedule, patient, item.getStart(), item.getEnd());
    }

    @POST
    @Path("/close-item")
    public void closeItem(DoctorItemDto doctorItemDto) {
        Item item = itemService.findById(doctorItemDto.getItemId());
        item.setResult(doctorItemDto.getResult());
        item.setRemark(doctorItemDto.getProcedure());
        item.setPast(true);
        itemService.save(item);
    }

    @GET
    @Path("/doctorSchedule")
    public String getDoctorSchedule(@QueryParam("username") String username) throws JsonProcessingException {
        Doctor doctor = doctorService.findByUsername(username);
        Schedule schedule = scheduleService.findByDoctor(doctor);
        List<ScheduleDto> doctorSchedulesDto = Scheduler.getDoctorSchedulesDto(schedule);
        return mapper.writeValueAsString(doctorSchedulesDto);
    }

    @GET
    @Path("/deleteItem")
    public void deleteItem(@QueryParam("itemId") String itemId) {
        itemService.removeById(Long.parseLong(itemId));
    }

    @GET
    @Path("/itemDetails")
    public String getItemDetails(@QueryParam("itemId") String itemId) throws JsonProcessingException {
        Item item = itemService.findById(Long.parseLong(itemId));
        return mapper.writeValueAsString(item);
    }

    @GET
    @Path("/find-patient")
    public String getPersonalCabinet(@QueryParam("username") String username) throws JsonProcessingException {
        Patient patient = patientService.findByUsername(username);
        return mapper.writeValueAsString(patient);
    }

    @GET
    @Path("/patient-items")
    public String getItems(@QueryParam("username") String username) throws JsonProcessingException {
        Patient patient = patientService.findByUsername(username);
        List<Item> items = itemService.findByPatient(patient);
        return mapper.writeValueAsString(items);
    }
}