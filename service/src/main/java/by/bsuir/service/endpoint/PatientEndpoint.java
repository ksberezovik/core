package by.bsuir.service.endpoint;

import by.bsuir.dao.persistent.entity.schedule.Item;
import by.bsuir.dao.persistent.entity.user.Patient;
import by.bsuir.service.service.ItemService;
import by.bsuir.service.service.PatientService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import java.util.List;

@Component
@Produces("application/json")
@Path("/patient")
public class PatientEndpoint {
    @Autowired
    PatientService patientService;
    @Autowired
    ItemService itemService;

    ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/find")
    public String getPersonalCabinet(@QueryParam("username") String username) throws JsonProcessingException {
        Patient patient = patientService.findByUsername(username);
        return mapper.writeValueAsString(patient);
    }

    @GET
    @Path("/items")
    public String getItems(@QueryParam("username") String username) throws JsonProcessingException {
        Patient patient = patientService.findByUsername(username);
        List<Item> items = itemService.findByPatient(patient);
        return mapper.writeValueAsString(items);
    }
}
