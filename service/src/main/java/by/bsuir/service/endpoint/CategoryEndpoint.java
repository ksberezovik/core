package by.bsuir.service.endpoint;

import by.bsuir.dao.persistent.entity.doctor.Category;
import by.bsuir.service.service.CategoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

@Component
@Produces("application/json")
@Consumes("application/json")
@Path("/categories")
public class CategoryEndpoint {

    @Autowired
    private CategoryService categoryService;

    ObjectMapper mapper = new ObjectMapper();

    @Path("/all")
    @GET
    public String getAllCategories() throws JsonProcessingException {
        List<Category> all = categoryService.findAll();
        return mapper.writeValueAsString(all);
    }
}
