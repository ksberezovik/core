package by.bsuir.service.endpoint;

import by.bsuir.dao.persistent.entity.user.ApplicationUser;
import by.bsuir.dao.persistent.entity.user.Doctor;
import by.bsuir.dao.persistent.entity.user.Patient;
import by.bsuir.service.service.DoctorService;
import by.bsuir.service.service.PatientService;
import by.bsuir.service.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

@Component
@Produces("application/json")
@Consumes("application/json")
@Path("/users")
public class UserEndpoint {

    @Autowired
    private UserService userService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/user")
    public String getUserByUserName(@QueryParam("username") String username) throws JsonProcessingException {
        ApplicationUser applicationUser = userService.findByUsername(username);
        return mapper.writeValueAsString(applicationUser);
    }

    @POST
    @Path("/doctor/signup")
    public void saveDoctor(Doctor doctor) {
        doctor.setPassword(bCryptPasswordEncoder.encode(doctor.getPassword()));
        doctorService.save(doctor);
    }

    @POST
    @Path("/patient/signup")
    public void savePatient(Patient patient) {
        patient.setPassword(bCryptPasswordEncoder.encode(patient.getPassword()));
        patientService.save(patient);
    }
}
