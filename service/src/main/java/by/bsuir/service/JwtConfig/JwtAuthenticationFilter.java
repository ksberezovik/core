package by.bsuir.service.JwtConfig;

import by.bsuir.dao.persistent.entity.user.ApplicationUser;
import by.bsuir.service.dto.SessionDto;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static by.bsuir.service.JwtConfig.SecurityConstants.*;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private ObjectMapper mapper = new ObjectMapper();
    private ApplicationUser applicationUser;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            applicationUser = readApplicationUserFromRequest(request);
            UsernamePasswordAuthenticationToken token = createToken(applicationUser);
            return authenticationManager.authenticate(token);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ApplicationUser readApplicationUserFromRequest(HttpServletRequest request) throws IOException {
        return new ObjectMapper().readValue(request.getInputStream(), ApplicationUser.class);
    }

    private UsernamePasswordAuthenticationToken createToken(ApplicationUser applicationUser) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                applicationUser.getUsername(),
                applicationUser.getPassword(),
                new ArrayList<>());
        return token;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException {
        String token = createJwtToken(authResult);
        SessionDto sessionDto = new SessionDto(TOKEN_PREFIX + token, applicationUser.getUsername());
        response.getWriter().write(mapper.writeValueAsString(sessionDto));
    }

    private String createJwtToken(Authentication authResult) {
        String token = JWT.create()
                .withSubject(((User) authResult.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
        return token;
    }
}