package by.bsuir.service.JwtConfig;

public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGetJWTs";
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_DOCTOR_URL = "/users/doctor/signup";
    public static final String SIGN_UP_PATIENT_URL = "/users/patient/signup";

    public static final String GET_ALL_CATEGORIES = "/categories/all";
}