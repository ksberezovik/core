package by.bsuir.dao;

import by.bsuir.dao.persistent.entity.assessment.Assessment;
import by.bsuir.dao.persistent.entity.common.Name;
import by.bsuir.dao.persistent.entity.doctor.Category;
import by.bsuir.dao.persistent.entity.user.ApplicationUser;
import by.bsuir.dao.persistent.entity.user.Doctor;
import by.bsuir.dao.persistent.entity.user.Patient;
import by.bsuir.dao.persistent.entity.schedule.Item;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.repository.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class PersistenceTest {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private ScheduleRepository scheduleRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private AssessmentRepository assessmentRepository;
    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    //@Test
    public void testSaveDoctorAndCategory() {
        Category category = new Category();
        category.setName("Surgeon");
        categoryRepository.save(category);

        Category surgeon = categoryRepository.findByName("Surgeon");
        assertNotNull(surgeon);

        Doctor doctor = new Doctor();
        doctor.setName(new Name("Natalia", "Petrovna", "Lutova"));

        Category doctorCategory = categoryRepository.findByName("Surgeon");

        doctor.setCategory(doctorCategory);
        doctorRepository.save(doctor);

        Set<Doctor> doctors = doctorRepository.findByCategoryId(1L);
        assertThat(doctors.size(), is(1));
        assertThat(doctors.iterator().next().getName().getFirstName(), is("Natalia"));
        assertThat(doctors.iterator().next().getCategory().getName(), is("Surgeon"));
    }

    //@Test
    public void saveDoctorAndSchedule() {
        Doctor doctor = new Doctor();
        doctor.setName(new Name("Galina", "Sergeevna", "Romashkina"));

        doctorRepository.save(doctor);

        Schedule schedule = new Schedule();
        schedule.setDoctor(doctor);

        scheduleRepository.save(schedule);

        Schedule scheduleByDoctor = scheduleRepository.findByDoctor(doctor);
        assertNotNull(scheduleByDoctor);
        assertThat(scheduleByDoctor.getDoctor().getName().getSecondName(), is("Romashkina"));
    }

    //@Test
    public void saveScheduleAndItems() {
        Schedule schedule = new Schedule();
        scheduleRepository.save(schedule);

        Item item1 = new Item();
        item1.setRemark("item1");
        item1.setSchedule(schedule);

        Item item2 = new Item();
        item2.setRemark("item2");
        item2.setSchedule(schedule);

        itemRepository.save(item1);
        itemRepository.save(item2);

        Optional<Schedule> scheduleOptional = scheduleRepository.findById(1L);
        assertNotNull(scheduleOptional.get());
        assertThat(scheduleOptional.get().getItems().size(), is(2));
    }

    //@Test
    public void testFindPatients() {
        Patient patient = new Patient();
        patient.setName(new Name("Ivan", "Ivanovich", "Ivanov"));
        patientRepository.save(patient);

        Patient patient2 = new Patient();
        patient2.setName(new Name("Ivan", "Ivanovich", "Ivanovski"));
        patientRepository.save(patient2);

        Patient patient3 = new Patient();
        patient3.setName(new Name("Ivan", "Ivanovich", "Ivanchenko"));
        patientRepository.save(patient3);

        Iterable<Patient> patients = patientRepository.findAll();
        assertNotNull(patients);
        assertThat(((List<Patient>) patients).size(), is(3));
    }

    //@Test
    public void saveAssessment() {
        Patient patient = new Patient();
        patient.setName(new Name("Illarion", "Ivanovich", "Kuco"));
        patientRepository.save(patient);

        Assessment assessment = new Assessment();
        assessment.setPatient(patient);
        assessment.setNote("test");
        assessmentRepository.save(assessment);

        Assessment assessment2 = new Assessment();
        assessment2.setPatient(patient);
        assessment2.setNote("test2");
        assessmentRepository.save(assessment2);

        List<Assessment> assessmentByPatient = assessmentRepository.findByPatient(patient);
        assertNotNull(assessmentByPatient);
        assertThat(assessmentByPatient.size(), is(2));
    }
}