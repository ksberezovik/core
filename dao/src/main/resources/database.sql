create table categories (id bigint auto_increment,
                         name varchar(100),
                         description varchar(10000),
                         primary key(id));

create table application_users(id bigint auto_increment,
                               username varchar(200),
                               password varchar(1000),
                               role varchar(50),
                               primary key (id));

create table doctors(application_user_id bigint,
                     first_name varchar(100),
                     middle_name varchar(100),
                     second_name varchar(100),
                     category_id bigint,
                     primary key (application_user_id),
                     foreign key (category_id) references categories(id),
                     foreign key (application_user_id) references application_users(id));

create table patients (application_user_id bigint,
                       first_name varchar(100),
                       middle_name varchar(100),
                       second_name varchar(100),
                       city varchar(100),
                       street varchar(100),
                       house varchar(50),
                       flat varchar(50),
                       primary key (application_user_id),
                       foreign key (application_user_id) references application_users(id));

create table carts(id bigint auto_increment,
                    patient_id bigint,
                    creation_date timestamp,
                    primary key (id),
                    foreign key (patient_id) references patients(application_user_id));

create table notes (id bigint auto_increment,
                    type varchar(200),
                    doctor_id bigint,
                    creation_date timestamp,
                    description varchar(500),
                    cart_id bigint,
                    primary key (id),
                    foreign key (doctor_id) references doctors(application_user_id),
                    foreign key (cart_id) references carts(id));

create table schedules (id bigint auto_increment,
                        doctor_id bigint,
                        primary key (id),
                        foreign key (doctor_id) references doctors(application_user_id));

create table items (id bigint auto_increment,
                    start_date timestamp,
                    end_date timestamp,
                    patient_id bigint,
                    schedule_id bigint,
                    remark varchar(500),
                    primary key (id),
                    foreign key (patient_id) references patients(application_user_id),
                    foreign key (schedule_id) references schedules(id));

create table contacts (id bigint auto_increment,
                      phone varchar(100),
                      email varchar(200),
                      patient_id bigint,
                      primary key (id),
                      foreign key (patient_id) references patients(application_user_id));

create table documents (id bigint auto_increment,
                        creation_date timestamp,
                        patient_id bigint,
                        primary key (id),
                        foreign key (patient_id) references patients(application_user_id));

create table templates(id bigint auto_increment,
                       primary key (id));

create table template_fields(id bigint auto_increment,
                             name varchar(200),
                             value varchar(200),
                             template_id bigint,
                             primary key (id),
                             foreign key (template_id) references templates(id));

create table assessments(id bigint auto_increment,
                         type varchar(100),
                         template_id bigint,
                         primary key (id),
                         foreign key (template_id) references templates(id));





