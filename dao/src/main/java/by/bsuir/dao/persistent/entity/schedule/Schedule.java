package by.bsuir.dao.persistent.entity.schedule;

import by.bsuir.dao.persistent.entity.common.BaseEntity;
import by.bsuir.dao.persistent.entity.user.Doctor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "schedules")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Schedule extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @OneToMany(mappedBy = "schedule", fetch = FetchType.EAGER)
    private List<Item> items = new ArrayList<>();
}
