package by.bsuir.dao.persistent.repository;

import by.bsuir.dao.persistent.entity.doctor.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    Category findByName(String name);
    Optional<Category> findById(Long id);
}
