package by.bsuir.dao.persistent.entity.assessment;

import by.bsuir.dao.persistent.entity.common.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "template_fields")
public class TemplateField extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private String value;

    @ManyToOne
    @JoinColumn(name = "template_id")
    private Template template;
}
