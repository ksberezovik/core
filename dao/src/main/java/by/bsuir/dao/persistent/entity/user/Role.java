package by.bsuir.dao.persistent.entity.user;

public enum Role {
    PATIENT,
    DOCTOR,
    ADMIN
}
