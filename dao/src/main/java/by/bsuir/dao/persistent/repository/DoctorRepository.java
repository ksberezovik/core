package by.bsuir.dao.persistent.repository;


import by.bsuir.dao.persistent.entity.doctor.Category;
import by.bsuir.dao.persistent.entity.user.Doctor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Long> {
    Set<Doctor> findByCategoryId(Long categoryId);
    Set<Doctor> findByCategory(Category category);
    Doctor findByUsername(String username);
}
