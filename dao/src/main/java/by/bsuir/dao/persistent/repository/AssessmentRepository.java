package by.bsuir.dao.persistent.repository;

import by.bsuir.dao.persistent.entity.assessment.Assessment;
import by.bsuir.dao.persistent.entity.user.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssessmentRepository extends CrudRepository<Assessment, Long> {
    List<Assessment> findByPatient(Patient patient);
}
