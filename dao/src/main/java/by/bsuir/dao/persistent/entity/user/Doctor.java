package by.bsuir.dao.persistent.entity.user;

import by.bsuir.dao.persistent.entity.common.Name;
import by.bsuir.dao.persistent.entity.doctor.Category;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Table(name = "doctors")
@PrimaryKeyJoinColumn(name = "application_user_id")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Doctor extends ApplicationUser {
    @Embedded
    private Name name;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "doctor_info")
    private String doctorInfo;

    @Column(name = "img")
    private String imgPath;

    @Override
    public Role getSystemRole() {
        return Role.DOCTOR;
    }
}