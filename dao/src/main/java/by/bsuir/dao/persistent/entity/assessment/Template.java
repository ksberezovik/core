package by.bsuir.dao.persistent.entity.assessment;

import by.bsuir.dao.persistent.entity.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "templates")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Template extends BaseEntity {

    @OneToMany(mappedBy = "template")
    List<TemplateField> templateFields = new ArrayList<>();
}
