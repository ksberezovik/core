package by.bsuir.dao.persistent.entity.cart;

import by.bsuir.dao.persistent.entity.common.BaseEntity;
import by.bsuir.dao.persistent.entity.user.Patient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "carts")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cart extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name = "creation_date")
    private Date creationDate;

    @OneToMany(mappedBy = "cart", orphanRemoval = true)
    private Set<Note> notes = new HashSet<>();
}