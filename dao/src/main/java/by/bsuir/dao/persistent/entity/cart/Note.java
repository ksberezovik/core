package by.bsuir.dao.persistent.entity.cart;

import by.bsuir.dao.persistent.entity.common.BaseEntity;
import by.bsuir.dao.persistent.entity.user.Doctor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notes")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Note extends BaseEntity {

    @Column(name = "type")
    private String type;

    @ManyToOne
    @JoinColumn(name = "doctor_id")
    private Doctor author;

    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "cart_id")
    private Cart cart;
}
