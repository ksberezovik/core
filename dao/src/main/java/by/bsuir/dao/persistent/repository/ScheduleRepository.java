package by.bsuir.dao.persistent.repository;

import by.bsuir.dao.persistent.entity.user.Doctor;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Long> {
    Schedule findByDoctor(Doctor doctor);
}
