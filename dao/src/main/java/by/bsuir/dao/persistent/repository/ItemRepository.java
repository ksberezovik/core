package by.bsuir.dao.persistent.repository;

import by.bsuir.dao.persistent.entity.schedule.Item;
import by.bsuir.dao.persistent.entity.schedule.Schedule;
import by.bsuir.dao.persistent.entity.user.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
    List<Item> findBySchedule(Schedule schedule);
    void removeById(Long id);
    List<Item> findByPatient(Patient patient);
}
