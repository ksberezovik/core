package by.bsuir.dao.persistent.entity.user;

import by.bsuir.dao.persistent.entity.cart.Cart;
import by.bsuir.dao.persistent.entity.common.Address;
import by.bsuir.dao.persistent.entity.common.Contacts;
import by.bsuir.dao.persistent.entity.common.Name;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "patients")
@PrimaryKeyJoinColumn(name = "application_user_id", referencedColumnName = "id")
@Getter
@Setter
@NoArgsConstructor
public class Patient extends ApplicationUser {
    @Embedded
    private Name name;

    @Embedded
    private Address address;

    @Embedded
    private Contacts contacts;

    @OneToOne(mappedBy = "patient")
    private Cart cart;

    @Override
    public Role getSystemRole() {
        return Role.PATIENT;
    }
}
