package by.bsuir.dao.persistent.entity.user;

import by.bsuir.dao.persistent.entity.common.BaseEntity;
import by.bsuir.dao.persistent.entity.common.Name;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "common_user")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@NoArgsConstructor
public class CommonUser extends BaseEntity {
}
