package by.bsuir.dao.persistent.repository;

import by.bsuir.dao.persistent.entity.user.CommonUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonUserRepository extends CrudRepository<CommonUser, Long> {
}
