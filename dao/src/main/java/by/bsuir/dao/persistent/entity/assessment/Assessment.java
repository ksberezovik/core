package by.bsuir.dao.persistent.entity.assessment;

import by.bsuir.dao.persistent.entity.common.BaseEntity;
import by.bsuir.dao.persistent.entity.user.Patient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "assessments")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Assessment extends BaseEntity {

    @Column(name = "type")
    private String type;

    @OneToOne
    @JoinColumn(name = "template_id")
    private Template template;

    @OneToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name = "note")
    private String note;
}
