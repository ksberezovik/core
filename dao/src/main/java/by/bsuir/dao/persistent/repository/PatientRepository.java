package by.bsuir.dao.persistent.repository;


import by.bsuir.dao.persistent.entity.user.Patient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {

    @Query(value = "SELECT p.* FROM patients p  WHERE LOWER(p.second_name) LIKE LOWER(concat(?1, '%'))", nativeQuery = true)
    List<Patient> findBySecondName(String secondName);
    Patient findByUsername(String username);
}
